import React from 'react';
import { Switch, Route } from 'react-router-dom';

import { Routes } from './services';
import { ErrorBoundary } from './components';
import { Home, SearchingWidget, UnavailableService } from '../src/components';

export class Router extends React.Component {
  render() {
    return (
      <ErrorBoundary>
        <Switch>
          <Route path={Routes.home} component={Home} exact />
          <Route path={Routes.searchingWidget} component={SearchingWidget} />
          <Route path={Routes.serviceWidget} component={UnavailableService} />
        </Switch>
      </ErrorBoundary>
    );
  }
}
