export const Breakpoints = {
  mobile: 420,
  desktop: 1140,
  tablet: 768,
};

export const showingTrackServiceButton = (breakpointWidth) => window.innerWidth <= breakpointWidth;
