import React from 'react';

import { SearchingWidget } from '../searching-widget';
import './home.css';
import { UnavailableService } from '../unavailable-service/';
import { NavLink } from 'react-router-dom';

export const Home = () => {
  return (
    <div className="main">
      <h1>Main page</h1>
      <NavLink to="/service">FE UX Developer Technical Task</NavLink>
      <p></p>
      <NavLink to="/searching">React Developer Technical Task</NavLink>
    </div>
  );
};
