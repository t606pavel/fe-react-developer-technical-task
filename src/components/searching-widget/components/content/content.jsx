import React from 'react';
import PropTypes from 'prop-types';

export const Content = ({ content }) => {
  return <div>{content}</div>;
};

Content.propTypes = {
  content: PropTypes.string.isRequired,
};
