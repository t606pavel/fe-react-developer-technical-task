import React from 'react';
import PropTypes from 'prop-types';

import './text-input.css';

export const TextInput = ({ inputValue, handleChangeInputValue, label }) => {
  return (
    <div className="text-input-width-label">
      <label>{label}</label>
      <input
        type="text"
        size="40"
        value={inputValue}
        onChange={(event) => handleChangeInputValue(event.target.value)}
      />
    </div>
  );
};

TextInput.propTypes = {
  inputValue: PropTypes.string.isRequired,
  handleChangeInputValue: PropTypes.func.isRequired,
  label: PropTypes.string,
};
