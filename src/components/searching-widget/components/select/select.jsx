import React from 'react';
import PropTypes from 'prop-types';

import { Label } from '../../../../services/api/types';
import './select.css';

export const Select = ({ items, hadleChangeSelectOption, chosenItem, labelCaption }) => {
  const onChangeItem = (event) => {
    const { value } = event.target;

    const chosenItem = items.find((item) => +value === item.id);

    hadleChangeSelectOption(chosenItem);
  };

  return (
    <label className="select-label">
      {labelCaption}
      <select
        onChange={(event) => onChangeItem(event)}
        value={chosenItem ? chosenItem.id : 'placeholder'}
      >
        <option value="placeholder" disabled hidden>
          {labelCaption}
        </option>
        {items.map((item, index) => (
          <option value={item.id} key={index}>
            {item.label}
          </option>
        ))}
      </select>
    </label>
  );
};

Select.propTypes = {
  items: PropTypes.arrayOf(Label),
  hadleChangeSelectOption: PropTypes.func,
  chosenItem: PropTypes.oneOfType([Label, PropTypes.instanceOf(null)]),
  labelCaption: PropTypes.string,
};
