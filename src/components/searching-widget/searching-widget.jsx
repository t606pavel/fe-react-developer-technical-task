import React, { Component } from 'react';

import { Button, Content, Select, TextInput } from './components';
import { getAllCategories, getAllPrices, getAllFields } from '../../services/';
import './searching-widget.css';
import { Spinner } from '../spinner';

export class SearchingWidget extends Component {
  state = {
    categories: [],
    fieldsLabels: [],
    prices: [],
    showAdditionalFilters: false,
    formFields: { searchPhrase: '', category: '', price: '' },
    searchPhrase: '',
    chosenCategory: null,
    chosenPrice: null,
    spinner: true,
  };

  fetchData = () => {
    try {
      const [fields, categories, prices] = [getAllFields(), getAllCategories(), getAllPrices()];
      const fieldsLabels = Object.values(fields).map((field) => field.value);

      this.setState({
        categories,
        fieldsLabels,
        prices,
        spinner: false,
      });
    } catch (error) {
      console.log(error);
    }
  };

  componentDidMount() {
    this.fetchData();
  }

  handleChangeCategory = (category) => {
    this.setState({ chosenCategory: category });
  };

  handleChangePrice = (price) => {
    this.setState({ chosenPrice: price });
  };

  handleChangeTextInput = (textInput) => {
    this.setState({ searchPhrase: textInput });
  };

  handleClickFilters = () => {
    this.setState((prevState) => ({ showAdditionalFilters: !prevState.showAdditionalFilters }));
  };

  handleClickApply = () => {
    const { chosenCategory, chosenPrice, searchPhrase } = this.state;
    this.setState({
      formFields: {
        searchPhrase,
        category: chosenCategory ? chosenCategory.label : '',
        price: chosenPrice ? chosenPrice.label : '',
      },
    });
  };

  handleClickDiscard = () => {
    this.setState({
      searchPhrase: '',
      chosenCategory: null,
      chosenPrice: null,
      formFields: { searchPhrase: '', category: '', price: '' },
    });
  };

  render() {
    const {
      categories,
      prices,
      showAdditionalFilters,
      searchPhrase,
      chosenCategory,
      chosenPrice,
      formFields,
      fieldsLabels,
      spinner,
    } = this.state;

    if (spinner) return <Spinner />;

    return (
      <div className="searching-widget">
        <div className="wrapper">
          <div className="common-field">
            <TextInput
              inputValue={searchPhrase}
              label={fieldsLabels[0]}
              handleChangeInputValue={this.handleChangeTextInput}
            />
            <Button title={fieldsLabels[1]} handleClick={this.handleClickFilters} />
          </div>
          {showAdditionalFilters && (
            <>
              <div className="selects">
                <Select
                  items={categories}
                  hadleChangeSelectOption={this.handleChangeCategory}
                  chosenItem={chosenCategory}
                  labelCaption={fieldsLabels[2]}
                />
                <Select
                  items={prices}
                  hadleChangeSelectOption={this.handleChangePrice}
                  chosenItem={chosenPrice}
                  labelCaption={fieldsLabels[3]}
                />
              </div>

              <div className="controls">
                <Button title={fieldsLabels[4]} handleClick={this.handleClickApply} />
                <Button title={fieldsLabels[5]} handleClick={this.handleClickDiscard} />
              </div>
            </>
          )}

          <div className="input-values">
            {formFields.searchPhrase && <Content content={formFields.searchPhrase} />}
            {formFields.category && <Content content={formFields.category} />}
            {formFields.price && <Content content={formFields.price} />}
          </div>
        </div>
      </div>
    );
  }
}

SearchingWidget.propTypes = {};
