import React from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleDown } from '@fortawesome/free-solid-svg-icons';
import { Doughnut } from 'react-chartjs-2';

import { Issue } from './components/';
import { Breakpoints } from '../../../../helpers';
import './request-info.css';

export const RequestTypes = {
  OPEN: 'Open',
  COMPLETED: 'Completed',
};

const MachineStatuses = {
  IMPACTED: 'impacted',
  BACK_IN_SERVICE: 'back in service',
  SERVICED: 'serviced',
};

const IssueTypes = {
  CRITICAL: 'Critical',
  NON_CRITICAL: 'Non-critical',
};

export const RequestInfo = ({ requestsType, duration, border, doughnutData }) => {
  const requestTypOpen = requestsType === RequestTypes.OPEN;
  const criticalIssueColor = requestTypOpen ? '#ed0b07' : '#006400';
  const nonCriticalIssueColor = requestTypOpen ? '#ed6866' : '#00c400';
  const hideDropdown = Breakpoints.desktop <= window.innerWidth;

  return (
    <div className={`request ${border}`}>
      <p className="request-header">
        <span className="request-type">{requestsType}</span> Requests
        <span className="request-duration"> last {duration} days</span>
      </p>

      <div className="request-info">
        <div>
          <Doughnut
            options={{
              rotation: 1 * Math.PI,
              circumference: 1 * Math.PI,

              animation: {
                animateRotate: false,
              },
            }}
            data={{
              datasets: [
                {
                  data: doughnutData,
                  backgroundColor: [criticalIssueColor, nonCriticalIssueColor],
                  borderWidth: 0,
                },
              ],
            }}
          />
        </div>
        <div className="request-issues">
          <Issue
            issueType={IssueTypes.CRITICAL}
            machinesAmount={3}
            amountIssues={4}
            color={criticalIssueColor}
            machineStatus={MachineStatuses.IMPACTED}
          />
          <Issue
            issueType={IssueTypes.NON_CRITICAL}
            color={nonCriticalIssueColor}
            machinesAmount={8}
            amountIssues={10}
            machineStatus={MachineStatuses.IMPACTED}
          />
        </div>
      </div>
      {hideDropdown && (
        <p>
          <span className="request-show">Show me</span> oldest open request{' '}
          <FontAwesomeIcon icon={faAngleDown} size="1x" className="angle-down" />
        </p>
      )}
    </div>
  );
};

RequestInfo.propTypes = {
  requestsType: PropTypes.string,
  duration: PropTypes.number,
  border: PropTypes.string,
  doughnutData: PropTypes.arrayOf(PropTypes.number),
};
