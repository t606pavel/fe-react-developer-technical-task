import React from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCircle } from '@fortawesome/free-solid-svg-icons';

import './issue.css';

export const Issue = ({ issueType, machinesAmount, amountIssues, color, machineStatus }) => {
  return (
    <div className="issue">
      <div className="issue-info">
        <p>
          <FontAwesomeIcon icon={faCircle} color={color} />
          {'  '}
          {amountIssues}
        </p>

        <p className="issue-type">{issueType}</p>
      </div>

      <p className="impacted-info">
        <span className="impacted-info-amount">{machinesAmount}</span> machines {machineStatus}
      </p>
    </div>
  );
};

Issue.propTypes = {
  issueType: PropTypes.string,
  machinesAmount: PropTypes.number,
  amountIssues: PropTypes.number,
  color: PropTypes.string,
  machineStatus: PropTypes.string,
};
