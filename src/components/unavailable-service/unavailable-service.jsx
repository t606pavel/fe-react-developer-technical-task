import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleRight, faPlus } from '@fortawesome/free-solid-svg-icons';
import { throttle as _throttle } from 'lodash';

import { RequestTypes } from './components/request-info/request-info';
import { RequestInfo } from './components';
import { Breakpoints } from '../../helpers';

import './unavailable-service.css';

export class UnavailableService extends React.Component {
  state = { doughnutData: [0, 1] };

  fetchData = () => {
    this.setState({ doughnutData: [4, 10] });
  };

  componentDidMount() {
    this.fetchData();
    window.addEventListener(
      'resize',
      _throttle(() => this.forceUpdate(), 300),
    );
  }

  componentWillUnmount() {
    window.removeEventListener(
      'resize',
      _throttle(() => this.forceUpdate(), 300),
    );
  }

  render() {
    const showingTrackServiceButton = Breakpoints.desktop <= window.innerWidth;
    const requestComponentBorder = showingTrackServiceButton ? 'border-right' : 'border-bottom';

    return (
      <div className="service">
        <div className="service-wrapper">
          <div className="service-header">
            <p>
              <span className="service-header-span">My Equipment</span>{' '}
              <FontAwesomeIcon
                icon={faAngleRight}
                size="1x"
                color="#ffffff"
                className="angle-right"
              />{' '}
              Unplanned Sevice
            </p>
            <button className="service-request-button">
              Create service request <FontAwesomeIcon icon={faPlus} size="1x" color="#ffffff" />
            </button>
          </div>

          <div className="service-requests">
            <RequestInfo
              requestsType={RequestTypes.OPEN}
              duration={90}
              border={requestComponentBorder}
              doughnutData={this.state.doughnutData}
            />
            <RequestInfo
              requestsType={RequestTypes.COMPLETED}
              duration={7}
              doughnutData={this.state.doughnutData}
            />
          </div>

          {!showingTrackServiceButton && (
            <button className="service-track-button">
              TRACK SERVICE <FontAwesomeIcon icon={faAngleRight} size="1x" color="blue" />{' '}
            </button>
          )}
        </div>
      </div>
    );
  }
}
