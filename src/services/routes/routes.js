export const Routes = {
  home: '/',
  error: '/error',
  searchingWidget: '/searching',
  serviceWidget: '/service',
};
