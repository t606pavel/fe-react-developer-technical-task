import PropTypes from 'prop-types';

export const Label = PropTypes.shape({ id: PropTypes.number, label: PropTypes.string });
